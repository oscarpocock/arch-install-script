# Arch Install Script
A simple Arch Linux install script, doesn't do much a the moment as it is very much in the testing phase.

# How to use:
Clone the repo:
```
git clone https://gitlab.com/oscarpocock/arch-install-script.git
```
Go inside the directory:
```
cd arch-install-script
````
Finally run the script:
```
bash install
```